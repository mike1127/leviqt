word = "banana"
# [ ("b", False), ("a
wordTuples = [(i.lower(), False) for i in list(word) ]

# guessed the letter a

def setLetter(letter, t):
    if t[0] == letter:
        return (t[0], True)
    return t

def checkIfComplete(w):
    return all([t[1] for t in w])


wordTuples = [setLetter("a", t) for t in wordTuples]
wordTuples = [setLetter("b", t) for t in wordTuples]
wordTuples = [setLetter("n", t) for t in wordTuples]

ds2 = []
for i in range(10):
    ds2.append(i)

# ds = [c[0] for c in d if c[1]]


def toRepresentation(t):
    return t[0] if t[1] else ' _ '

rep = [t[0] if t[1] else ' _ ' for t in wordTuples]
print("".join(rep))
print(checkIfComplete(wordTuples))
