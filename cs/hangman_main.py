
def checkIfComplete(w):
    return all([t[1] for t in w])


def checkIfIncomplete(w):
    return any([not t[1] for t in w])


def toRepresentation(t):
    return t[0] if t[1] else ' _ '


def showRepresentation(wordTuples):
    rep = [t[0] if t[1] else ' _ ' for t in wordTuples]
    print("".join(rep))


def main():
    word = "banana"
    wordTuples = [(i.lower(), False) for i in list(word)]
    while checkIfIncomplete(wordTuples):
        showRepresentation(wordTuples)
        inp = input("Type of charactor: ")
        wordTuples = [(c, True) if c == inp else (c, f)
                      for (c, f) in wordTuples]
    showRepresentation(wordTuples)
    print("You win!")


main()
