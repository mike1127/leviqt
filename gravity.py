from PyQt5.QtWidgets import QWidget, QHBoxLayout, QApplication
from PyQt5.QtCore import QPointF, QSize, QTimer, Qt, QElapsedTimer
from PyQt5.QtGui import QPalette, QPen, QPainter
import math

class RenderArea(QWidget):
    def __init__(self, parent=None):
        super(RenderArea, self).__init__(parent) 
        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)
        self.antialiased = True

        # self.rotation = 0.0
        # self.t = 0.0
        # self.deltaT = -1.0
        # self.radius = 100.0
        # self.center = QPointF(200, 200)

    def minimumSizeHint(self):
        return QSize(400, 400)

    def sizeHint(self):
        return QSize(750, 750)

    def paintEvent(self, event):
        pass

    def timeUpdate(self, t):
        self.t = t
        self.update()


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.renderArea = RenderArea()
        lo = QHBoxLayout()
        lo.addWidget(self.renderArea)
        self.setLayout(lo)
        self.setWindowTitle("Drawing")
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()
        
        self.timer = QTimer(self)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.timeUpdate)
        self.timer.start(30)

    def keyPressEvent(self, evt):
        pass
    

    def timeUpdate(self):
        self.renderArea.timeUpdate(self.elapsedTimer.elapsed())
        
        
        
if __name__ == '__main__':
    app = QApplication([])
    win = Window()
    win.show()
    app.exec_()
